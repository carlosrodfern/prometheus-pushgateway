# How to build locally

```sh
spectool -g prometheus-pushgateway.spec
rpkg srpm --outdir .
mock -r centos-stream-9-x86_64 --init
mock --enable-network -r centos-stream-9-x86_64 *.src.rpm
```

# How to test

```sh
cp /var/lib/mock/centos-stream-9-x86_64/result/prometheus-pushgateway-1.8.0-1.el9.x86_64.rpm ./tests/data/prometheus-pushgateway.rpm
tmt -vvv run
```
