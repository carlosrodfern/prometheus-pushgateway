# https://github.com/prometheus/pushgateway
%global goipath         github.com/prometheus/pushgateway
Version:                1.8.0
%global tag             v1.8.0

%gometa -f

%global goname prometheus-pushgateway

%global common_description %{expand:
Push acceptor for ephemeral and batch jobs.}

%global golicenses      LICENSE NOTICE\\\
                        resources/static/bootstrap-4.3.1-dist/LICENSE
%global godocs          CHANGELOG.md CODE_OF_CONDUCT.md CONTRIBUTING.md\\\
                        MAINTAINERS.md README.md SECURITY.md

Name:           %{goname}
Release:        1%{?dist}
Summary:        Push acceptor for ephemeral and batch jobs

License:        Apache-2.0
URL:            %{gourl}
Source0:        %{gosource}
Source1:        %{goname}.sysusers
Source2:        %{goname}.service
Source3:        %{goname}.conf

BuildRequires:  systemd-rpm-macros
Requires(pre): shadow-utils

%description %{common_description}

%gopkg

%prep
%goprep -k
%autopatch -p1

%build
export GO111MODULE=on
%gobuild -o %{gobuilddir}/bin/%{goname} %{goipath}

%install
install -m 0755 -vd                     %{buildroot}%{_bindir}
install -m 0755 -vp %{gobuilddir}/bin/* %{buildroot}%{_bindir}/
install -m 0644 -vpD %{S:1} %{buildroot}%{_sysusersdir}/%{goname}.conf
install -m 0644 -vpD %{S:2} %{buildroot}%{_unitdir}/%{goname}.service
install -m 0644 -vpD %{S:3} %{buildroot}%{_sysconfdir}/default/%{goname}
mkdir -vp %{buildroot}/%{_mandir}/man1/
%{buildroot}%{_bindir}/%{goname} --help-man > %{buildroot}/%{_mandir}/man1/%{goname}.1
sed -i '/^  /d; /^.SH "NAME"/,+1c.SH "NAME"\nprometheus-pushgateway \\- The Prometheus Pushgateway' \
    %{buildroot}/%{_mandir}/man1/%{goname}.1

%pre
%sysusers_create_compat %{SOURCE1}

%post
%systemd_post %{goname}.service

%preun
%systemd_preun %{goname}.service

%postun
%systemd_postun_with_restart %{goname}.service

%files
%license LICENSE NOTICE resources/static/bootstrap-4.3.1-dist/LICENSE
%doc CHANGELOG.md CODE_OF_CONDUCT.md CONTRIBUTING.md MAINTAINERS.md README.md
%doc SECURITY.md
%{_bindir}/*
%config(noreplace) %{_sysconfdir}/default/%{goname}
%{_unitdir}/%{goname}.service
%{_sysusersdir}/%{goname}.conf
%{_mandir}/man1/%{goname}.1*

%changelog
%autochangelog
