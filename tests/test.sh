#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart
    rlPhaseStartSetup
        rlRun "dnf install -y ./data/prometheus-pushgateway.rpm"
        rlServiceStart "prometheus-pushgateway.service"
    rlPhaseEnd

    rlPhaseStartTest
        rlWaitForCmd "curl -s http://localhost:9091/-/healthy" -t 60
        rlRun -s "curl -s -w '%{http_code}' http://localhost:9091/-/healthy"
        rlAssertGrep "200" $rlRun_LOG
    rlPhaseEnd

    rlPhaseStartCleanup
        rlServiceStop "prometheus-pushgateway.service"
        rlRun "dnf remove -y prometheus-pushgateway"
    rlPhaseEnd
rlJournalEnd
